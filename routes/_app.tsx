import { asset, Head } from "$fresh/runtime.ts";
import { AppProps } from "$fresh/src/server/types.ts";
import DarkMode from "../islands/dark-mode.tsx";

export default function App({ Component }: AppProps) {
  const code = `function global_dark(change) {
    if (change === 'auto') delete localStorage.theme; else if (change === 'on') localStorage.theme = 'dark'; else if (change === 'off') localStorage.theme = 'light';
    window.isDark = localStorage.theme === "dark" || (!("theme" in localStorage) && window.matchMedia("(prefers-color-scheme: dark)").matches)
    document.documentElement.classList[window.isDark ? 'add' : 'remove']("dark");
  }
  global_dark();`;
  return (
    <html data-custom="data">
      <Head>
        <title>Page de status de l'Incubateur des Territoires</title>
        <link rel="stylesheet" href={asset("/global.css")} />
        <script
          dangerouslySetInnerHTML={{
            __html: code,
          }}
        />
      </Head>
      <body class="dark:bg-dark">
        <div>
          <header class="py-8 md:py-12 mb-8">
            <div class="container flex flex-col items-center md:flex-row justify-between">
              <div class="mb-8 md:mb-0 bg-white">
                <a href="/">
                  <img
                    src={asset("/logo.svg")}
                    alt="Incubateur des territoires"
                    width="256"
                  />
                </a>
              </div>
              <div class="links flex space-x-2 items-center leading-none font-medium">
                <DarkMode prev={"system"} />
                <a
                  href="https://gitlab.com/incubateur-territoires/devops/infrastructure/-/issues/new?issuable_template=incident&issue%5Bissue_type%5D=incident"
                  target="_blank"
                  class="inline-flex items-center px-4 py-2 border border-gray-300 dark:border-dark rounded-md shadow-sm text-sm font-medium text-gray-700 dark:text-gray-100 bg-white dark:bg-gray-800 hover:bg-gray-50 dark:hover:bg-gray-700 focus:outline-none focus:ring-2"
                >
                  <svg
                    class="-ml-1 mr-2 h-5 w-5"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 22 22"
                    fill="currentColor"
                    aria-hidden="true"
                  >
                    <path
                      d="M11 15h2v2h-2v-2m0-8h2v6h-2V7m1-5C6.47 2 2 6.5 2 12a10 10 0 0 0 10 10a10 10 0 0 0 10-10A10 10 0 0 0 12 2m0 18a8 8 0 0 1-8-8a8 8 0 0 1 8-8a8 8 0 0 1 8 8a8 8 0 0 1-8 8z"
                      fill="currentColor"
                    >
                    </path>
                  </svg>
                  Créer un incident
                </a>
              </div>
            </div>
          </header>
          <main>
            <Component />
          </main>
        </div>
      </body>
    </html>
  );
}
