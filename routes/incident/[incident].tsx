import IncidentComponent from "../../components/Incident.tsx";
import { getIncident } from "../../api/incidents.ts";
import { Handlers, PageProps } from "$fresh/server.ts";
import type { Incident } from "../../api/incidents.ts";

export const handler: Handlers<Incident> = {
  async GET(_req, ctx) {
    const incident = await getIncident(parseInt(ctx.params.incident, 10));
    if (!incident) {
      return new Response("Incidents not found", { status: 404 });
    }
    return ctx.render(incident);
  },
};
export default function Home(props: PageProps<Incident>) {
  return (
    <section class="incidents py-4">
      <h2 className="container text-2xl font-semibold tracking-tight text-gray-900 dark:text-gray-100">
        Incident
      </h2>
      <IncidentComponent incident={props.data} />
    </section>
  );
}
