import AvailabilityBarChart from "../components/AvailabilityBarChart.tsx";
import IncidentsList from "../components/IncidentsList.tsx";
import { Handlers, PageProps } from "$fresh/server.ts";
import type { Incident } from "../api/incidents.ts";
import { listIncidents } from "../api/incidents.ts";
import AvailabilityChecker, {
  getAvailabilityEndpoints,
} from "../api/availability-checker.ts";

const checkers = getAvailabilityEndpoints().map((endpoint) => ({
  ...endpoint,
  checker: new AvailabilityChecker(
    endpoint.url,
    10 * 60 * 1000,
  ),
}));

export const handler: Handlers<Incident[]> = {
  async GET(_req, ctx) {
    const incidents = await listIncidents();
    if (!incidents) {
      return new Response("Incidents not found", { status: 404 });
    }
    return ctx.render(incidents);
  },
};
export default function Home(props: PageProps<Incident[]>) {
  return (
    <>
      <h2 class="container text-xs tracking-wide text-gray-500 dark:text-gray-300 uppercase font-bold mb-8">
        Monitors
      </h2>
      <section class="monitors space-y-6">
        {checkers.map((checker) => (
          <AvailabilityBarChart
            label={checker.label}
            checker={checker.checker}
          />
        ))}
      </section>
      <section class="incidents py-16">
        <IncidentsList incidents={props.data} />
      </section>
    </>
  );
}
