# Status page

## Usage

Lancer le projet :

```
deno task start
```

## Variables d'environnement

L'application prend en charge les variables d'environnement suivantes pour
définir les points de terminaison de disponibilité et les paramètres GitLab :

### Points de terminaison de disponibilité

- URL : `AVAILABILITY_CHECK_<identifiant>`
- Label (optionnel) : `AVAILABILITY_CHECK_<identifiant>_LABEL`

L'`<identifiant>` doit être un identifiant unique pour chaque paire de variables
d'environnement URL et label.

### Paramètres GitLab

- GITLAB_ACCESS_TOKEN : le jeton d'accès à GitLab, utilisé pour s'authentifier
  auprès de l'API GitLab.
- GITLAB_INCIDENTS_PROJECT_NUMBER : le numéro du projet GitLab où les incidents
  sont enregistrés.

## Exemple de configuration

Exemple d'un ensemble de variables d'environnement :

```
AVAILABILITY_CHECK_site1=https://www.example.com
AVAILABILITY_CHECK_site1_LABEL=Exemple
AVAILABILITY_CHECK_site2=https://www.example2.com
GITLAB_ACCESS_TOKEN=abcdefghijklm
GITLAB_INCIDENTS_PROJECT_NUMBER=12345
```
