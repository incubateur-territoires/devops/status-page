import "https://deno.land/x/reflection@0.0.2/mod.ts";

import {
  dconf,
  hydrate,
  hydrationConfig,
} from "https://deno.land/x/dconf@v1.0.0/mod.ts";

import { load as dotEnvConfig } from "https://deno.land/std@v0.182.0/dotenv/mod.ts";
await dotEnvConfig({ export: true });

@hydrationConfig({
  prefixCli: "gitlab-",
  prefixEnv: "GITLAB_",
})
class GitlabConfig {
  @hydrate()
  incidentsProjectNumber: number | null = null;
  @hydrate()
  ApiURL = "https://gitlab.com/api/v4";
  @hydrate()
  accessToken: string | null = null;
}

class ConfigMap {
  public gitlab: GitlabConfig = dconf(GitlabConfig);
}

export default dconf(ConfigMap);
