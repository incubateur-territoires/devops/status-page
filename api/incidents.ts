import conf from "../config.ts";
import { camelCase } from "https://deno.land/x/case@2.1.1/mod.ts";

function normalizeObject(
  // deno-lint-ignore no-explicit-any
  object: any,
  // deno-lint-ignore no-explicit-any
): any {
  if (typeof object !== "object" || object === null || object instanceof Date) {
    return object;
  }
  // deno-lint-ignore no-explicit-any
  let camelizedObject: any;
  if (Array.isArray(object)) {
    camelizedObject = object.map((item) => normalizeObject(item));
  } else {
    camelizedObject = {};
    Object.entries(object).forEach(([key, value]) => {
      camelizedObject[camelCase(key)] = normalizeObject(value);
    });
  }
  return camelizedObject;
}

if (
  !conf.gitlab.ApiURL || !conf.gitlab.incidentsProjectNumber ||
  !conf.gitlab.accessToken
) {
  throw new Error("A gitlab parameter seems to be unknown.");
}

export enum IncidentState {
  Opened = "opened",
  Closed = "closed",
}

export enum IncidentSeverity {
  Unknown = "UNKNOWN",
  Low = "LOW",
  Medium = "MEDIUM",
  High = "HIGH",
  Critical = "CRITICAL",
}

export interface Comment {
  body: string;
  createdAt: Date;
  updatedAt: Date;
}
export interface Incident {
  iid: number;
  title: string;
  description: string;
  createdAt: Date;
  updatedAt: Date;
  closedAt: Date;
  state: IncidentState;
  severity: IncidentSeverity;
  isFull: boolean;
  comments: Comment[];
  webUrl: string;
  labels: string[];
}

function normalizeIncident(incident: Incident, isFull = false) {
  incident = normalizeObject(incident);
  return {
    ...incident,
    createdAt: new Date(incident.createdAt),
    updatedAt: new Date(incident.updatedAt),
    closedAt: new Date(incident.closedAt),
    isFull,
  } as Incident;
}

function normalizeComment(comment: Comment) {
  comment = normalizeObject(comment);
  return {
    ...comment,
    createdAt: new Date(comment.createdAt),
    updatedAt: new Date(comment.updatedAt),
  } as Comment;
}

export async function listIncidents() {
  const incidentsUrl =
    `${conf.gitlab.ApiURL}/projects/${conf.gitlab.incidentsProjectNumber}/issues?issue_type=incident&access_token=${conf.gitlab.accessToken}&sort=desc&order_by=created_at`;

  const response = await fetch(incidentsUrl);
  if (!response.ok) {
    throw new Error(`Failed to fetch incidents: ${response.statusText}`);
  }

  return (await response.json()).map((i: Incident) => normalizeIncident(i));
}

export async function getIncident(id: number) {
  const incidentUrl =
    `${conf.gitlab.ApiURL}/projects/${conf.gitlab.incidentsProjectNumber}/issues/${id}?access_token=${conf.gitlab.accessToken}`;

  const response = await fetch(incidentUrl);
  if (!response.ok) {
    throw new Error(`Failed to fetch incidents: ${response.statusText}`);
  }
  const commentsUrl =
    `${conf.gitlab.ApiURL}/projects/${conf.gitlab.incidentsProjectNumber}/issues/${id}/notes?access_token=${conf.gitlab.accessToken}&order_by=created_at`;
  const commentsResponse = await fetch(commentsUrl);

  if (!commentsResponse.ok) {
    throw new Error(
      `Failed to fetch comments for incident #${id}: ${commentsResponse.statusText}`,
    );
  }
  const incident = await response.json();
  incident.comments = (await commentsResponse.json()).filter((c: Comment) =>
    c.body.startsWith("🎤")
  ).map(normalizeComment);
  return normalizeIncident(incident, true);
}
