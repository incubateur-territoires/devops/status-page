export interface AvailabilityDayPair {
  day: string;
  availability: number;
}
interface AvailabilityResult {
  day: string;
  count: number;
  available: number;
}
export default class AvailabilityChecker {
  public results: AvailabilityResult[] = [];
  constructor(
    public url: string,
    public checkInterval = 60 * 60 * 1000,
    public maxDays = 30,
  ) {
    this.results = [];
    this.startChecking();
  }

  startChecking() {
    this.checkAvailability();
    setInterval(() => this.checkAvailability(), this.checkInterval);
  }

  async checkAvailability() {
    try {
      const response = await fetch(this.url, {
        headers: {
          "X-Robots-Tag": "Status-Page-ANCT-Incubateur",
        },
      });
      const isAvailable = response.status < 400;
      this.addResult(isAvailable);
    } catch (error) {
      console.error("Error while checking availability:", error);
      this.addResult(false);
    }
  }

  addResult(isAvailable: boolean) {
    const now = new Date();
    const day = now.toISOString().substring(0, 10);
    const existingEntry = this.results.find((entry) => entry.day === day);

    if (existingEntry) {
      existingEntry.count++;
      if (isAvailable) {
        existingEntry.available++;
      }
    } else {
      if (this.results.length >= this.maxDays) {
        this.results.shift();
      }
      this.results.push({
        day: day,
        count: 1,
        available: isAvailable ? 1 : 0,
      });
    }
  }

  getAvailability(): AvailabilityDayPair[] {
    return this.results.map((entry) => ({
      day: entry.day,
      availability: (entry.available / entry.count) * 100,
    }));
  }

  getGlobalAvailability() {
    const total = this.results.reduce(
      (accumulator, entry) => accumulator + entry.count,
      0,
    );
    const available = this.results.reduce(
      (accumulator, entry) => accumulator + entry.available,
      0,
    );
    return (available / total) * 100;
  }
}

interface AvailabilityEndpoint {
  label: string;
  url: string;
}

const parseAvailabilityEndpoints = (
  obj: { [key: string]: string },
): AvailabilityEndpoint[] => {
  const results: AvailabilityEndpoint[] = [];
  const urlPattern = /^AVAILABILITY_CHECK_([A-Za-z0-9]+)$/;
  const labelSuffix = "_LABEL";

  for (const key in obj) {
    const match = key.match(urlPattern);
    if (match) {
      const urlKey = key;
      const labelKey = key + labelSuffix;
      const url = obj[urlKey];
      const label = obj[labelKey] || new URL(url).hostname;

      results.push({ label, url });
    }
  }

  return results.filter((item) => item.url !== "");
};

export function getAvailabilityEndpoints() {
  return parseAvailabilityEndpoints(Deno.env.toObject());
}
