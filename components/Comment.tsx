import { FunctionalComponent, h } from "preact";
import { html, tokens } from "https://deno.land/x/rusty_markdown/mod.ts";
import type { Comment } from "../api/incidents.ts";

const CommentComponent: FunctionalComponent<{
  comment: Comment;
}> = ({ comment }) => {
  const body = html(tokens(comment.body, { strikethrough: true }));
  return (
    <>
      <div
        class="mt-3"
        dangerouslySetInnerHTML={{
          __html: body,
        }}
      />
      <div class="text-gray-500">
        {comment.createdAt.toLocaleDateString(undefined, {
          year: "numeric",
          month: "long",
          day: "numeric",
          hour: "2-digit",
          minute: "2-digit",
          second: "2-digit",
        })}
      </div>
    </>
  );
};

export default CommentComponent;
