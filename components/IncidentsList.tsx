import { FunctionalComponent } from "preact";
import type { Incident } from "../api/incidents.ts";
import IncidentComponent from "../components/Incident.tsx";

const IncidentsList: FunctionalComponent<{
  incidents: Incident[];
}> = (
  { incidents },
) => {
  return (
    <>
      <h2 class="container text-2xl font-semibold tracking-tight text-gray-900 dark:text-gray-100">
        Incidents
      </h2>
      {incidents.length === 0
        ? (
          <div class="container incidents text-gray-500 dark:text-gray-400">
            Aucun incident.
          </div>
        )
        : (
          incidents.map((incident) => (
            <div
              class="py-8 dark:bg-gray-700 bg-opacity-10 mt-12"
              key={incident.iid}
            >
              <IncidentComponent incident={incident} />
            </div>
          ))
        )}
    </>
  );
};

export default IncidentsList;
