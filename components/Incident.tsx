import { FunctionalComponent } from "preact";
import type { Incident } from "../api/incidents.ts";
import { IncidentSeverity, IncidentState } from "../api/incidents.ts";

import Comment from "./Comment.tsx";

const IncidentComponent: FunctionalComponent<{
  incident: Incident;
}> = ({ incident }) => {
  let state;
  switch (incident.state) {
    case IncidentState.Opened:
      state = {
        color: "bg-red-400 dark:bg-red-800",
        label: "Ouvert",
      };
      break;
    case IncidentState.Closed:
      state = {
        color: "bg-green-400 dark:bg-green-800",
        label: "Clos",
      };
      break;
    default:
      state = {
        color: "bg-gray-400 dark:bg-gray-800",
        label: "Inconnu",
      };
      break;
  }

  let severity;
  switch (incident.severity) {
    case IncidentSeverity.Low:
      severity = {
        color: "bg-red-100 dark:bg-red-300",
        label: "Basse",
      };
      break;
    case IncidentSeverity.Medium:
      severity = {
        color: "bg-red-200 dark:bg-red-500",
        label: "Moyenne",
      };
      break;
    case IncidentSeverity.High:
      severity = {
        color: "bg-red-300 dark:bg-red-700",
        label: "Haute",
      };
      break;
    case IncidentSeverity.Critical:
      severity = {
        color: "bg-red-400 dark:bg-red-900",
        label: "Critique",
      };
      break;
    default:
      severity = {
        color: "bg-gray-400 dark:bg-gray-800",
        label: "Inconnue",
      };
      break;
  }

  return (
    <div class="container text-gray-700 dark:text-gray-300">
      <div class="inline-block bg-gray-100 dark:bg-gray-800 rounded-md px-2 font-medium pb-1 mb-2 text-gray-900 dark:text-gray-300">
        {incident.createdAt.toLocaleDateString(undefined, {
          year: "numeric",
          month: "long",
          day: "numeric",
        })}
      </div>
      <h3 class="text-blue-700 dark:text-blue-400 text-xl font-semibold">
        <a href={incident.webUrl} target="_blank">{incident.title}</a>
      </h3>
      <span class="float-right inline-flex items-center">
        <span class="inline-block rounded-l-md px-2 font-medium text-white dark:text-black bg-gray-700 dark:bg-white ml-1">
          État
        </span>
        <span
          class={`inline-block rounded-r-md px-2 font-medium text-gray-800 dark:text-gray-300 mr-1 ${state.color}`}
        >
          {state.label}
        </span>
      </span>
      <span class="float-right inline-flex items-center">
        <span class="inline-block rounded-l-md px-2 font-medium text-white dark:text-black bg-gray-700 dark:bg-white ml-1">
          Sévérité
        </span>
        <span
          class={`inline-block rounded-r-md px-2 font-medium text-gray-800 dark:text-gray-300 mr-1 ${severity.color}`}
        >
          {severity.label}
        </span>
      </span>
      {incident.labels.length > 0 &&
        (
          <span class="float-right inline-flex items-center">
            <span class="inline-block rounded-l-md px-2 font-medium text-white dark:text-black bg-gray-700 dark:bg-white ml-1">
              Labels
            </span>
            <span class="inline-block rounded-r-md px-2 font-medium bg-gray-300 text-gray-800 dark:text-gray-300 mr-1 dark:bg-gray-800">
              {incident.labels.join(", ")}
            </span>
          </span>
        )}
      <p class="mt-3 incidents text-gray-500 dark:text-gray-400">
        {incident.description}
      </p>
      {!!incident.isFull &&
        incident.comments.map((c) => <Comment comment={c} />)}
      <p class="mt-3 incidents dark:text-gray-50 font-bold">
        Dernière mise à jour,{" "}
        {incident.updatedAt.toLocaleDateString(undefined, {
          year: "numeric",
          month: "long",
          day: "numeric",
          hour: "2-digit",
          minute: "2-digit",
          second: "2-digit",
        })}
      </p>
      {!incident.isFull && (
        <a
          href={`/incident/${incident.iid}`}
          className="text-right block text-gray-900 dark:text-white"
        >
          Détail
          <svg
            class="inline-block fill-current text-gray-900 dark:text-white"
            xmlns="http://www.w3.org/2000/svg"
            width="64"
            height="64"
            viewBox="-10 0 100 100"
          >
            <path
              d="M 9.518 37.866 H 1 c -0.552 0 -1 -0.448 -1 -1 s 0.448 -1 1 -1 h 8.518 c 0.552 0 1 0.448 1 1 S 10.07 37.866 9.518 37.866 z"
              style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill-rule: nonzero; opacity: 1;"
              transform=" matrix(1 0 0 1 0 0) "
              stroke-linecap="round"
            />
            <path
              d="M 57.872 77.188 c -0.603 0 -1.206 -0.229 -1.665 -0.688 l -9.587 -9.587 c -0.918 -0.92 -0.918 -2.414 0 -3.332 l 9.447 -9.447 H 29.181 c -0.552 0 -1 -0.447 -1 -1 s 0.448 -1 1 -1 h 29.301 c 0.404 0 0.77 0.243 0.924 0.617 c 0.155 0.374 0.069 0.804 -0.217 1.09 L 48.034 64.995 c -0.139 0.139 -0.139 0.365 0.001 0.505 l 9.585 9.585 c 0.139 0.14 0.365 0.139 0.504 0 l 29.78 -29.781 c 0.084 -0.084 0.099 -0.191 0.096 -0.266 c 0.003 -0.15 -0.012 -0.257 -0.096 -0.341 l -29.78 -29.781 c -0.181 -0.18 -0.321 -0.184 -0.504 0 l -9.586 9.585 c -0.139 0.139 -0.139 0.365 0 0.504 l 11.154 11.153 c 0.286 0.286 0.372 0.716 0.217 1.09 c -0.154 0.374 -0.52 0.617 -0.924 0.617 H 18.341 c -0.552 0 -1 -0.448 -1 -1 s 0.448 -1 1 -1 h 37.727 l -9.447 -9.447 c -0.918 -0.919 -0.918 -2.414 0 -3.332 l 9.586 -9.586 c 0.892 -0.891 2.444 -0.89 3.332 0 l 29.78 29.78 c 0.464 0.463 0.705 1.087 0.68 1.755 c 0.025 0.593 -0.216 1.216 -0.68 1.68 l -29.78 29.781 C 59.079 76.958 58.476 77.188 57.872 77.188 z"
              style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill-rule: nonzero; opacity: 1;"
              transform=" matrix(1 0 0 1 0 0) "
              stroke-linecap="round"
            />
            <path
              d="M 20.862 54.134 h -8.295 c -0.552 0 -1 -0.447 -1 -1 s 0.448 -1 1 -1 h 8.295 c 0.552 0 1 0.447 1 1 S 21.414 54.134 20.862 54.134 z"
              style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill-rule: nonzero; opacity: 1;"
              transform=" matrix(1 0 0 1 0 0) "
              stroke-linecap="round"
            />
            <path
              d="M 41.533 46 H 7.307 c -0.552 0 -1 -0.448 -1 -1 s 0.448 -1 1 -1 h 34.226 c 0.552 0 1 0.448 1 1 S 42.085 46 41.533 46 z"
              style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill-rule: nonzero; opacity: 1;"
              transform=" matrix(1 0 0 1 0 0) "
              stroke-linecap="round"
            />
          </svg>
        </a>
      )}
    </div>
  );
};

export default IncidentComponent;
