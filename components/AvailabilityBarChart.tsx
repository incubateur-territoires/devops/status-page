import { FunctionalComponent, h } from "preact";
import AvailabilityChecker, {
  AvailabilityDayPair,
} from "../api/availability-checker.ts";

enum Status {
  OK = "green",
  WARNING = "yellow",
  KO = "red",
}

const component: FunctionalComponent<{
  label: string;
  checker: AvailabilityChecker;
}> = ({ label, checker }) => {
  const availability = checker.getAvailability();
  const latest = availability.at(-1);
  if (!latest) {
    return <></>;
  }
  const color = latest.availability > 90
    ? Status.OK
    : (latest.availability > 70 ? Status.WARNING : Status.KO);
  return (
    <div class={`monitor py-8 bg-${color}-400 bg-opacity-10`}>
      <div class="container flex items-center justify-between mb-3">
        <h3 class={`text-2xl text-gray-800 dark:text-gray-100`}>{label}</h3>
        <span class={`text-${color}-600 dark:text-${color}-400 font-semibold`}>
          {color === Status.OK && "Opérationnel"}
          {color === Status.WARNING && "Dégradé"}
          {color === Status.KO && "En alerte"}
        </span>
      </div>
      <div class="container bars">
        <div class="flex space-x-px">
          {availability.map((r: AvailabilityDayPair) => (
            <div
              class={`bars bg-${
                r.availability > 90
                  ? Status.OK
                  : (r.availability > 70 ? Status.WARNING : Status.KO)
              }-400 flex-1 h-10 rounded hover:opacity-80 cursor-pointer`}
              title={`${r.availability}%, le ${r.day}`}
            >
            </div>
          ))}
        </div>
      </div>
      <div class="container mt-2">
        <div class="flex items-center">
          <span
            class={`pr-2 flex-shrink-0 text-${color}-500 text-xs font-semibold`}
          >
            Il y a {checker.results?.length} jours
          </span>
          <div class={`h-px bg-${color}-500 w-full`}></div>
          <span
            class={`px-2 flex-shrink-0 text-${color}-500 text-xs font-semibold`}
          >
            {checker.getGlobalAvailability()}%
          </span>
          <div class={`h-px bg-${color}-500 w-full`}></div>
          <span
            class={`pl-2 flex-shrink-0 text-${color}-500 text-xs font-semibold`}
          >
            Aujourd'hui
          </span>
        </div>
      </div>
    </div>
  );
};
export default component;
