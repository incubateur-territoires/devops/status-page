import { FunctionalComponent } from "preact";
import { useState } from "preact/hooks";
import { IS_BROWSER } from "$fresh/runtime.ts";

interface DarkModeProps {
  prev: "light" | "dark" | "system";
}

const DarkMode: FunctionalComponent<DarkModeProps> = (props: DarkModeProps) => {
  function updateMode() {
    const w = window as unknown as { isDark: boolean };
    w.isDark = localStorage.theme === "dark" ||
      (!("theme" in localStorage) &&
        window.matchMedia("(prefers-color-scheme: dark)").matches);
    document.documentElement.classList[w.isDark ? "add" : "remove"]("dark");
  }
  const toggleDarkMode = () => {
    console.log(
      !("theme" in localStorage) &&
        window.matchMedia("(prefers-color-scheme: dark)").matches,
    );
    localStorage.theme = localStorage.theme === "dark" ? "light" : "dark";
    updateMode();
  };
  return (
    <button
      type="button"
      onClick={toggleDarkMode}
      class="inline-flex items-center p-2 border border-gray-300 dark:border-dark rounded-md shadow-sm text-sm font-medium text-gray-700 dark:text-gray-100 bg-white dark:bg-gray-800 hover:bg-gray-50 dark:hover:bg-gray-700 focus:outline-none focus:ring-2"
    >
      <svg
        class="h-5 w-5"
        xmlns="http://www.w3.org/2000/svg"
        fill="currentColor"
        viewBox="0 0 24 24"
        aria-hidden="true"
        style="display: none;"
      >
        <path
          stroke-linecap="round"
          stroke-linejoin="round"
          stroke-width="2"
          d="M20.354 15.354A9 9 0 018.646 3.646 9.003 9.003 0 0012 21a9.003 9.003 0 008.354-5.646z"
        >
        </path>
      </svg>
      <svg
        class="h-5 w-5"
        xmlns="http://www.w3.org/2000/svg"
        stroke="currentColor"
        viewBox="0 0 24 24"
        aria-hidden="true"
      >
        <path
          stroke-linecap="round"
          stroke-linejoin="round"
          stroke-width="2"
          d="M12 3v1m0 16v1m9-9h-1M4 12H3m15.364 6.364l-.707-.707M6.343 6.343l-.707-.707m12.728 0l-.707.707M6.343 17.657l-.707.707M16 12a4 4 0 11-8 0 4 4 0 018 0z"
        >
        </path>
      </svg>
    </button>
  );
};

export default DarkMode;
